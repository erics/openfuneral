@extends('master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <signature-form></signature-form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
	      <p>PS: Si vous avez des jolies photos merci de nous les envoyer directement sur le serveur ou par courrier électronique ça nous ferait vraiment plaisir !</p>
            </div>
        </div>
    </div>
@endsection
