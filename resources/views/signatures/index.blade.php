@extends('master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
	<div style="padding: 10px;">
	  <p>Ce site internet étant accessible à tout le monde vous y trouverez des témoignages de gens qui ne pourront pas se déplacer à <a href="{{ route('bye') }}">la cérémonie</a>.<br />Si vous avez envie de laisser un petit mot vous pouvez le faire en <a href="{{ route('sign') }}">suivant ce lien</a>.</p>
	<p>Un message sympa</p>
    <p class="text-justify" style="font-style: italic;">Nous espérons que ces quelques lignes évoqueront des bons souvenirs communs. N'hésitez pas à nous raconter ces «&nbsp; moments de vie &nbsp;» en <a href="{{ route('sign') }}">complétant ce petit livre d'or électronique</a> ... </p>
    <p style="text-align: right;font-style: italic;">Sa famille</p>

	</div>      
      </div>      
    </div>
  </div>
</div>
@endsection
