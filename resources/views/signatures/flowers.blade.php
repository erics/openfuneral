@extends('master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-12">
	<div class="panel panel-default">
	  <div style="padding: 10px;">
	    <h2>Avant d'envoyer des fleurs</h2>
	    <p class="text-justify">Veuillez lire ces quelques lignes ...</p>
	    <p class="text-justify">Elle souhaitait être incinérée, les fleurs vont donc nous encombrer plus qu'autre chose ... de plus la symbolique que représente le fait de couper des plantes ne correspond pas avec la manière de voir la vie de xxxxxx, laissons donc ces jolies fleurs en terre être butinées et participer au grand cycle de la vie au lieu de les couper pour être vendues, puis jetées une fois fanées.</p>
	    
	    <p class="text-justify">Votre argent peut-être utile en soutenant des causes importantes qui lui tenaient à coeur, vous pouvez donc à votre discretion :</p>
	    <ul>
	      <li>Participer à la cagnotte en ligne en faveur de xxxxxxxx.</li>
	      <li>Faire un don à l'association xxxxxxxxxx</li>
	    </ul>
	    <h2>Et si vraiment vous voulez envoyer des fleurs</h2>
	    <p>Prennez en photo les fleurs que vous voulez et envoyez cette photo sur l'album commémoratif, comme ça vos fleurs seront partagées avec tout le monde et pourront continuer leur vie sauvage (ou domestique si vous avez des belles plantes chez vous).</p>
	  </div>
	</div>
      </div>
    </div>
  </div>
</div>
@endsection
