<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Votre témoignage - un petit mot ...</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet" type="text/css">
    <script src="//cdn.ckeditor.com/4.5.5/standard/ckeditor.js"></script> 
  </head>
  <body>
    <div id="app">
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
	<a class="navbar-brand" href="{{ route('home') }}">Accueil</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Menu">
	  <span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
	  <ul class="navbar-nav mr-auto">
	    <li class="nav-item"><a class="nav-link" href="{{ route('list') }}">Lire les messages</a></li>
	    <li class="nav-item"><a class="nav-link" href="{{ route('flowers') }}">Envoyer des fleurs</a></li>
	    <li class="nav-item"><a class="nav-link" href="{{ route('bye') }}">La cérémonie</a></li>
	    <li class="nav-item"><a class="nav-link" href="{{ route('sign') }}">Laisser un petit mot</a></li>
	    <li class="nav-item"><a class="nav-link" href="{{ route('photo') }}">Album photo</a></li>
	  </ul>
	</div>
      </nav>
      
      @yield('content')
      
    </div>
    <div style="clear:both">
      <br /> &nbsp; <hr /> <br />
      <footer class="site-footer">
	<div style="text-align: center">Le code source de ce site web est disponible sur <a href="https://framagit.org/erics/openfuneral/">le dépôt suivant</a>.</div>
      </footer>
    </div>
    <script src="{{ mix('js/app.js') }}"></script>
    <script language="javascript">
    (function() {
    var b = document.getElementById('body');
    if(b) {
    CKEDITOR.config.toolbar = [
      ['Bold','Italic','Underline','-','Cut','Copy','Paste','-', 'NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock']
    ] ;
      var cke_body = CKEDITOR.replace('body', {
        language: 'fr',
        removePlugins: 'sourcearea'
      });
      cke_body.on('change', function(ev) {
        b.value = cke_body.getData();
      });
      cke_body.on('blur', function(ev) {
        b.value = cke_body.getData();
        console.log(b.value);
    });
    }
    })();
    </script>
  </body>
</html>
