<?php
use Sabre\DAV\Client;
use App\Models\Image;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('signatures', 'Api\SignatureController')
    ->only(['index', 'store', 'show']);


Route::resource('photos', 'Api\PhotoController')
    ->only(['index']);

Route::get('photos/show/{fic}', function ($fic) {

    //Si le fichier existe on le passe direct
    $localfile  = public_path() . "album/$fic";
    $localthumb = public_path() . "/thumb/$fic";
    if(! file_exists($localfile)) {
        $settings = array(
            'baseUri' => env('APP_PHOTO_NEXTCLOUD_BASEURI'),
            'userName' => env('APP_PHOTO_NEXTCLOUD_PUBLICSHARE'),
            'password' => ''
        );

        $client = new Client($settings);
        
        $response = $client->request('GET',"/public.php/webdav/$fic");
        
        $content = $response["body"];
        file_put_contents($localfile,$content);
    }
    
    if(! file_exists($localthumb)) {
        // Creation d'un thumbnail 200x200
        list($width, $height) = getimagesize($localfile);
        if ($width > $height) {
            $y = 0;
            $x = ($width - $height) / 2;
            $smallestSide = $height;
        } else {
            $x = 0;
            $y = ($height - $width) / 2;
            $smallestSide = $width;
        }
        $thumb = imagecreatetruecolor(200, 200);
        $source = imagecreatefromjpeg($localfile);
        imagecopyresampled($thumb, $source, 0, 0, $x, $y, 200, 200, $smallestSide, $smallestSide);
        imagejpeg($thumb,$localthumb);
    }
    
    $content = file_get_contents($localthumb);
    return $content;
});

//Tout ce qui suit est "protégé"
Route::group(['middleware' => 'auth:api'], function() {
    Route::put('signatures/{signature}/report', 'Api\ReportSignature@update');
});